(function($){

    function initialize_field( $el ) {
        /* global ajaxurl, current_site_id, isRtl */
        /* This function borrowed from wp-admin/js/user-suggest.js */

        /// TODO: Use $el instead of matching with $('div.pixo-autocomplete input')
        var position = { offset: '0, -1' };
        if ( typeof isRtl !== 'undefined' && isRtl ) {
            position.my = 'right top';
            position.at = 'right bottom';
        }
        $( 'div.pixo-autocomplete input' ).each( function(index){
            var $this = $(this),
                key = $this.parents('.pixo-autocomplete').data('key');

            $this.autocomplete({
                source:     ajaxurl + '?action=uic-ahs-autocomplete-profiles&key=' + key,
                delay:      500,
                minLength:  2,
                position:   position,
                open: function() {
                    $(this).addClass('open');
                },
                close: function() {
                    $(this).removeClass('open');
                },
                focus: function( event, ui ) {
                    $this.val(ui.item.label);
                    return false;
                },
                select: function( event, ui ) {
                    $this.val(ui.item.label);
                    let selector = getValueElementSelector($(this), key);
                    $(selector).val(ui.item.value);
                    return false;
                }
            })
                .autocomplete("instance")._renderItem = function(ul, item) {
                return $('<li>')
                    .append( '<div>' + item.label + '</div>')
                    .appendTo(ul);
            };
        });

    }

    function getValueElementSelector(autocomplete_instance, key) {
        let id = autocomplete_instance.attr('id');
        return 'input[name="custom-autocomplete['+key+']['+id+']"]';
    }

    if( typeof acf.add_action !== 'undefined' ) {

        /*
         *  ready append (ACF5)
         *
         *  These are 2 events which are fired during the page load
         *  ready = on page load similar to $(document).ready()
         *  append = on new DOM elements appended via repeater field
         *
         *  @type	event
         *  @date	20/07/13
         *
         *  @param	$el (jQuery selection) the jQuery element which contains the ACF fields
         *  @return	n/a
         */

        acf.add_action('ready append', function( $el ){

            // search $el for fields of type 'custom_autocomplete'
            acf.get_fields({ type : 'custom_autocomplete'}, $el).each(function(){
                initialize_field( $(this) );
            });

        });


    } else {


        /*
         *  acf/setup_fields (ACF4)
         *
         *  This event is triggered when ACF adds any new elements to the DOM.
         *
         *  @type	function
         *  @since	1.0.0
         *  @date	01/01/12
         *
         *  @param	event		e: an event object. This can be ignored
         *  @param	Element		postbox: An element which contains the new HTML
         *
         *  @return	n/a
         */

        $(document).on('acf/setup_fields', function(e, postbox){

            $(postbox).find('.field[data-field_type="custom_autocomplete"]').each(function(){

                initialize_field( $(this) );

            });

        });


    }


})(jQuery);
