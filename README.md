# ACF Custom Autocomplete Field

Adds autocomplete field, Custom Autocomplete, to Advanced Custom Fields designed to pull data from a custom db table, search on and select from specified db columns.  Requires >= v5.0.0 Advanced Custom Fields.

-----------------------

### Description

Adds autocomplete field, Custom Autocomplete, to Advanced Custom Fields designed to pull data from a custom db table, search on and select from specified db columns.  Requires >= v5.0.0 Advanced Custom Fields.

### Compatibility

This ACF field type is compatible with:
* ACF 5

### Installation

1. Copy the `custom-autocomplete` folder into your `wp-content/plugins` folder
2. Activate the Custom Autocomplete plugin via the plugins admin page
3. Create a new field via ACF and select the Custom Autocomplete type
4. Please refer to the Configuration section for further steps.

### Configuration

The following settings should be set for each instance of this field:

* *Data Source* is the database table name that serves as the source for your autocomplete options.
* *DB Columns for search* are the columns that should be searched for matching strings to what users type into the autocomplete field.
* *DB Columns to select* are the columns selected for the db query result.

By default, the response is formatted by setting the value to the first column selected, and then imploding the rest with ', ' as the glue.  To override this functionality, use one of these filter hooks, depending on how specific you want to be: 

* `pixo_shared_autocomplete_post_format_results`
* `pixo_shared_autocomplete_post_format_results/db_table={$this->table_name}` where you target fields with the value of their *Data Source* config field.
* `pixo_shared_autocomplete_post_format_results/name={$this->field['name']}` where you target fields with the name of the field instance.
* `pixo_shared_autocomplete_post_format_results/key={$this->field['key']}` where you target fields with the field key.

### Changelog
Please see [CHANGELOG](/CHANGELOG.md/) for changelog. 
