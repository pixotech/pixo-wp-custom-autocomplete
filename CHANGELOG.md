# Changelog for Pixo Custom Autocomplete

## 1.3.3

Clear up some PHP notices/errors and add some unit tests.

## 1.3.2

Bug fix for ACF v5.5.0 -- custom-autocomplete inside repeater doesn't save first time because ID is a clone index instead.

## 1.3.1

Bug fix for ACF v5.5.0 -- `$field['id']` no longer available to the `acf/prepare_field/type={type}` filter.

## 1.3.0

Add support for aribtrary where clause statement in field definition, and support for a limit on query.

## 1.2.1

Add filter hook for modifying part of where clause and limit in SQL query.

## 1.2.0

Fix support for custom autocomplete fields being in a repeater field.

## 1.1.4

Clean up how labels are created and IDs are selected and used.  

## 1.1.3

input.js should depend on jquery-ui-autocomplete JS script being loaded.

## 1.1.2

Change default label column to 'title'.

## 1.1.1

Bugfix: Autocomplete didn't work inside repeater field.

## 1.1.0

Skipped some versions, but this fixes a problem with showing labels in field, storing the id value, 
and sending the full referenced object to the API and templates.  It also adds new settings fields:

* value_id -- no default
* id_column -- default: 'id'
* label_column -- default: 'post_title'

## 0.2.0

First beta release.