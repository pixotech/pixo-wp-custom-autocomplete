<?php

require_once __DIR__ . '/src/Autoloader.php';

spl_autoload_register(new \WordPress\Pixo\SharedAutoComplete\Autoloader('WordPress\\Pixo\\SharedAutoComplete', __DIR__ . '/src/'));