<?php

//emulate WP environment
define('ABSPATH', 'test');
require 'wordpress/docroot/wp-content/plugins/advanced-custom-fields-pro/core/field.php';
function __($args){}
function add_action($args){}
function add_filter($args){}

/**
 * To run:
 * in vm: cd /var/www/uicahs
 * ./vendor/bin/phpunit --testsuite autocomplete
 */
class EventTest extends \PHPUnit_Framework_TestCase
{
    public $settings = [];

    /**
     * @test
     */
    public function repeaterAutoCompleteField()
    {
        require_once __DIR__ . "/../fields/acf-custom_autocomplete-v5.php";

        $post = [
            "acf" => [
                "field_profile_first_name" => "aaaaa",
                "field_profile_middle_name" => "",
                "field_profile_last_name" => "aaaaa",
                "field_profile_type" => "faculty",
                "field_profile_image_required1000" => "239",
                "field_profile_image_required1000_alt" => "aft",
                "field_profile_titles_and_homes_repeater" => [
                    [
                        "field_profile_titles_and_homes_content_title" => "",
                        "field_profile_titles_and_homes_content_home" => "Assistive Technology Unit"
                    ]
                ],
                "field_573ce94b87a23_ProfileContact1000" => "sdasd@asdasd",
                "field_573ce95a87a24_ProfileContactRequired1000" => "sdsds",
                "field_profile_tty" => "",
                "field_autocomplete_building_required" => "Assembly Hall",
                "field_profile_room_number_required" => "sdsd",
                "field_profile_office_mail_code_required" => "sdsds",
                "field_profile_related_site" => "",
                "field_profile_office_hours" => "By appointment",
                "field_profile_cv" => "",
                "field_programs_repeater" => [
                    [
                        "field_profile_programs" => "PhD in BBQ"
                    ]
                ],
                "field_profile_about_required" => "asdasda",
                "field_profile_focus_areas" => "",
                "field_profile_selected_grants" => "",
                "field_profile_selected_pubs" => "",
                "field_profile_publication_aggregators" => "",
                "field_profile_community_service" => "",
                "field_profile_leadership" => "",
                "field_profile_honors" => "",
                "field_profile_education" => "",
                "field_dont_aggregate_profile" => ""
            ],
            "custom-autocomplete" => [
                "field_autocomplete_building_required" => [
                    "acf-field_autocomplete_building_required" => "2"
                ],
                "field_profile_programs" => [
                    "acf-field_programs_repeater-0-field_profile_programs" => "1" /* This is the expected value */
                ]
            ],
            "user_ID" => 1,
            "action" => "editpost",
            "post_author" => 1,
            "post_type" => "profile",
            "post_ID" => "268",
            "post_name" => "aaaaa-aaaaa",
            "ID" => 268,
            "post_parent" => 0
        ];
        $field = [
            "key" => "field_profile_programs",
            "parent" => "field_programs_repeater",
            "ID" => 0,
            "label" => "Program",
            "name" => "profile_programs_repeater_0_program",
            "prefix" => "acf",
            "type" => "custom_autocomplete",
            "value" => null,
            "menu_order" => 0,
            "instructions" => "Start typing to select a program.",
            "required" => false,
            "id" => "",
            "class" => "",
            "conditional_logic" => 0,
            "_name" => "program",
        ];
        $value = "PhD in BBQ";

        $acf = new \acf_field_custom_autocomplete([]);

        $actualValue = $acf->getValueInRepeater($value, $post, $field);
        $expectedValue = "1";

        $this->assertEquals($expectedValue, $actualValue);
    }

    /**
     * @test
     */
    public function repeaterAutoCompleteFieldInsideFlexibleContentLayout()
    {
        require_once __DIR__ . "/../fields/acf-custom_autocomplete-v5.php";

        $post = [
            "acf" => [
                "field_56c29a68f8490" => "student work highlights",
                "field_574612e4bc790" => "intro text",
                "field_57055baa0949c_1000" => [
                    [
                        "acf_fc_layout" => "freeform_section",
                        "field_5705454e36b0e_1000" => "Test Section",
                        "flexible_content_body_content_components" => [
                            [
                                "acf_fc_layout" => "student_work_highlights_component",
                                "field_student_work_autocomplete_repeater" => [
                                    [
                                        "field_student_work_autocomplete" => "Student Work Test BBQ"
                                    ],
                                    [
                                        "field_student_work_autocomplete" => "Student Work Test Indian"
                                    ],
                                    [
                                        "field_student_work_autocomplete" => "Student Work Test BBQ"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "custom-autocomplete" => [
                "field_student_work_autocomplete" => [
                    "acf-field_57055baa0949c_1000-0-flexible_content_body_content_components-0-field_student_work_autocomplete_repeater-0-field_student_work_autocomplete" => "1",
                    "acf-field_57055baa0949c_1000-0-flexible_content_body_content_components-0-field_student_work_autocomplete_repeater-1-field_student_work_autocomplete" => "2", //this is the expected value
                    "acf-field_57055baa0949c_1000-0-flexible_content_body_content_components-0-field_student_work_autocomplete_repeater-2-field_student_work_autocomplete" => "1"
                ]
            ],
            "metakeyinput" => "",
            "metavalue" => "",
            "post_name" => "student-work-test",
            "ID" => 257,
            "post_parent" => 0
        ];
        $field = [
            "return_format" => "object",
            "value_id" => "",
            "id_column" => "id",
            "key" => "field_student_work_autocomplete",
            "label" => "Student Work",
            "name" => "section_0_content_components_0_student_work_highlights_repeater_0_student_work",
            "prefix" => "acf",
            "type" => "custom_autocomplete",
            "value" => null,
            "menu_order" => 0,
            "instructions" => "Start typing to find a student work story by title. Stories without a teaser image will not be returned.",
            "required" => true,
            "id" => "",
            "class" => "",
            "conditional_logic" => 0,
            "parent" => "field_student_work_autocomplete_repeater",
            "_name" => "student_work",
            "_prepare" => 0,
            "_valid" => 1
        ];
        $value = "Student Work Test Indian";

        $acf = new \acf_field_custom_autocomplete([]);

        $actualValue = $acf->getValueInRepeater($value, $post, $field);
        $expectedValue = "2";
        
        /**
         * Marty 2016-12-07
         * As I understand the current functionality, we'd need to implement a getValueInRepeater that crawls the acf
         * until it finds the field key, while building up a concatenated list of the keys used to get there. Then it
         * can pull the right value out of the "custom-autocomplete" array for the matching super-key.
         */
        $this->assertEquals($expectedValue, $actualValue, "This is the test to fix to get Student Work Highlights working!");
    }

    /**
     * @test
     */
    public function singleAutoCompleteField()
    {
        require_once __DIR__ . "/../fields/acf-custom_autocomplete-v5.php";

        $post = [
            "action" => "editpost",
            "post_ID" => "123",
            "acf" => [
                "field_profile_first_name" => "aaaaa",
                "field_profile_middle_name" => "",
                "field_profile_last_name" => "aaaaa",
                "field_profile_type" => "faculty",
                "field_profile_image_required1000" => "239",
                "field_profile_image_required1000_alt" => "aft",
                "field_profile_titles_and_homes_repeater" => [
                    [
                        "field_profile_titles_and_homes_content_title" => "",
                        "field_profile_titles_and_homes_content_home" => "Assistive Technology Unit"
                    ]
                ],
                "field_573ce94b87a23_ProfileContact1000" => "sdasd@asdasd",
                "field_573ce95a87a24_ProfileContactRequired1000" => "sdsds",
                "field_profile_tty" => "",
                "field_autocomplete_building_required" => "Assembly Hall",
                "field_profile_room_number_required" => "sdsd",
                "field_profile_office_mail_code_required" => "sdsds",
                "field_profile_related_site" => "",
                "field_profile_office_hours" => "By appointment",
                "field_profile_cv" => "",
                "field_programs_repeater" => [
                    [
                        "field_profile_programs" => "PhD in BBQ"
                    ]
                ],
                "field_profile_about_required" => "asdasda",
                "field_profile_focus_areas" => "",
                "field_profile_selected_grants" => "",
                "field_profile_selected_pubs" => "",
                "field_profile_publication_aggregators" => "",
                "field_profile_community_service" => "",
                "field_profile_leadership" => "",
                "field_profile_honors" => "",
                "field_profile_education" => "",
                "field_dont_aggregate_profile" => ""
            ],
            "custom-autocomplete" => [
                "field_autocomplete_building_required" => [
                    "acf-field_autocomplete_building_required" => "2" //This is the value we expect
                ],
                "field_profile_programs" => [
                    "acf-field_programs_repeater-0-field_profile_programs" => "1"
                ]
            ]
        ];

        $field = [
            "ID" => 0,
            "key" => "field_autocomplete_building_required",
            "label" => "Office Building",
            "name" => "building",
            "prefix" => "acf",
            "type" => "custom_autocomplete",
            "value" => null,
            "menu_order" => 15,
            "instructions" => "Start typing to select a building.",
            "required" => true,
            "id" => "",
            "class" => "",
            "parent" => "group_profile"
        ];

        $acf = new \acf_field_custom_autocomplete([]);

        $actualValue = $acf->getValueInSingle($post, $field);
        $expectedValue = "2";

        $this->assertEquals($expectedValue, $actualValue);
    }
}
