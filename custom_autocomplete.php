<?php

/*
Plugin Name: Advanced Custom Fields: Custom Autocomplete
Plugin URI: http://www.pixotech.com
Description: Adds autocomplete field, Custom Autocomplete, to Advanced Custom Fields designed to pull data from a custom db table, search on and select from specified db columns.  Requires >= v5.0.0 Advanced Custom Fields.
Version: 1.3.3
Author: Copyright (C) 2016 On the Job Consulting Inc. d/b/a Pixo
Author URI: http://www.pixotech.com
License: BSD-3
License URI: https://opensource.org/licenses/BSD-3-Clause
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;

require_once __DIR__ . '/init.php';


// check if class already exists
if( !class_exists('acf_plugin_custom_autocomplete') ) :

class acf_plugin_custom_autocomplete {
	
	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct() {
		
		// vars
		$this->settings = array(
			'version'	=> '1.3.3',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);
		
		
		// set text domain
		// https://codex.wordpress.org/Function_Reference/load_plugin_textdomain
		load_plugin_textdomain( 'acf-custom_autocomplete', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' ); 
		
		
		// include field
		add_action('acf/include_field_types', 	array($this, 'include_field_types')); // v5
		add_action('acf/register_fields', 		array($this, 'include_field_types')); // v4
		
	}
	
	
	/*
	*  include_field_types
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	n/a
	*/
	
	function include_field_types( $version = false ) {
		
		// support empty $version
		if( !$version ) $version = 4;
		
		
		// include
		include_once('fields/acf-custom_autocomplete-v' . $version . '.php');
		
	}
	
}


// initialize
new acf_plugin_custom_autocomplete();


// class_exists check
endif;
	
?>