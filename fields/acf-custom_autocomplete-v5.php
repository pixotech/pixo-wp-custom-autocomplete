<?php

use WordPress\Pixo\SharedAutoComplete\AjaxAutoComplete;

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_field_custom_autocomplete') ) :


class acf_field_custom_autocomplete extends acf_field {
    
    
    /*
    *  __construct
    *
    *  This function will setup the field type data
    *
    *  @type    function
    *  @date    5/03/2014
    *  @since   5.0.0
    *
    *  @param   n/a
    *  @return  n/a
    */
    
    function __construct( $settings ) {
        
        $this->name = 'custom_autocomplete';
        
        
        /*
        *  label (string) Multiple words, can include spaces, visible when selecting a field type
        */
        
        $this->label = __('Custom Autocomplete', 'acf-custom_autocomplete');
        
        
        /*
        *  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
        */
        
        $this->category = 'relational';
        
        
        /*
        *  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
        */
        
        $this->defaults = array(
            'taxonomy'          => array(),
            'allow_null'        => 0,
            'multiple'          => 0,
            'return_format'     => 'object',
            'custom_db_table'   => '',
            'db_columns'        => '',
            'db_columns_select' => '',
            'value_id'          => '',
            'id_column'         => 'id',
            'label_column'      => 'title',
            'where_filter'      => '',
            'limit'             => 20,
            'maxlength'         => false,
            'placeholder'       => ''
        );
        
        
        /*
        *  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
        *  var message = acf._e('custom_autocomplete', 'error');
        */
        
        $this->l10n = array(
            'error' => __('Error! Please enter a higher value', 'acf-custom_autocomplete'),
        );
        
        
        /*
        *  settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
        */
        
        $this->settings = $settings;
        
        // do not delete!
        parent::__construct();
        add_action('wp_ajax_uic-ahs-autocomplete-profiles', [$this, 'createAutoCompleteHandler'], 2);
        add_filter('acf/prepare_field/type=custom_autocomplete', [$this, 'prepareFieldFilter']);
    }

    public static function prepareFieldFilter($field)
    {
        $field['wrapper']['data-source'] = $field['custom_db_table'];
        $field['wrapper']['data-properties'] = $field['db_columns'];
        $field['wrapper']['class'] = 'pixo-autocomplete';
        $field['prepend'] = '';
        $field['value_id'] = $field['value'];
        $field['append'] = self::createHiddenIdField($field);
        $field['value'] = self::getLabelFromId($field);
        return $field;
    }

    public static function createHiddenIdField($field)
    {
        $id = str_replace(array('][', '[', ']'), array('-', '-', ''), $field['name']);
        $return = '<input type="hidden" name="custom-autocomplete[' . $field['key'] . '][' . $id . ']"';
        if(isset($field['value']))
            $return .= ' value="' . $field['value'] . '"';
        $return .= '>';

        return $return;
    }

    public static function getLabelFromId($field)
    {
        global $wpdb;
        $sql = $wpdb->prepare("SELECT {$field['label_column']} FROM {$field['custom_db_table']} WHERE {$field['id_column']} = %d", $field['value_id']);
        $label = $wpdb->get_col($sql);
        if(empty($label))
            return false;
        return $label[0];
    }

    public static function createAutoCompleteHandler()
    {
        $field_obj = get_field_object($_GET['key']);
        return new AjaxAutoComplete($_GET['term'], $field_obj);
    }    
    
    /*
    *  render_field_settings()
    *
    *  Create extra settings for your field. These are visible when editing a field
    *
    *  @type    action
    *  @since   3.6
    *  @date    23/01/13
    *
    *  @param   $field (array) the $field being edited
    *  @return  n/a
    */
    
    function render_field_settings( $field ) {
        
        /*
        *  acf_render_field_setting
        *
        *  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
        *  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
        *
        *  More than one setting can be added by copy/paste the above code.
        *  Please note that you must also have a matching $defaults value for the field name (font_size)
        */
        
        acf_render_field_setting( $field, [
            'label'         => __('Data Source','acf-custom_autocomplete'),
            'instructions'  => __('Database table name','acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'custom_db_table',
        ]);
        acf_render_field_setting( $field, [
            'label'         => __('DB Columns for search','acf-custom_autocomplete'),
            'instructions'  => __('List only the columns you want to search on, separated by commas.','acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'db_columns',
        ]);
        acf_render_field_setting( $field, [
            'label'         => __('DB Columns to select','acf-custom_autocomplete'),
            'instructions'  => __('List only the columns you want the db query to select, separated by commas.','acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'db_columns_select',
        ]);
        acf_render_field_setting( $field, [
           'label'         => __('DB table ID column','acf-custom_autocomplete'),
            'instructions'  => __('Type the column name of the id column in your custom DB table.  Default is "id".','acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'id_column', 
        ]);
        acf_render_field_setting( $field, [
            'label'         => __('DB table label column','acf-custom_autocomplete'),
            'instructions'  => __('Type the column name that should be the label of each item in your custom DB table.  Default is "post_name".','acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'label_column', 
        ]);
        acf_render_field_setting( $field, [
            'label'         => __('Additional filter to be placed in search query.', 'acf-custom_autocomplete'),
            'instructions'  => __('Type the SQL to be placed in the where clause leaving out "WHERE", for example "teaser_image IS NOT NULL".', 'acf-custom_autocomplete'),
            'type'          => 'text',
            'name'          => 'where_filter'
        ]);
        acf_render_field_setting( $field, [
            'label'         => __('Limit to number of results returned.', 'acf-custom_autocomplete'),
            'instructions'  => __('Enter integer', 'acf-custom_autocomplete'),
            'type'          => 'int',
            'name'          => 'limit'
        ]);
    }
    
    
    
    /*
    *  render_field()
    *
    *  Create the HTML interface for your field
    *
    *  @param   $field (array) the $field being rendered
    *
    *  @type    action
    *  @since   3.6
    *  @date    23/01/13
    *
    *  @param   $field (array) the $field being edited
    *  @return  n/a
    */
    
    function render_field( $field ) {

        $field['type'] = 'text';

        acf_render_field( $field );
    }
    
        
    /*
    *  input_admin_enqueue_scripts()
    *
    *  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
    *  Use this action to add CSS + JavaScript to assist your render_field() action.
    *
    *  @type    action (admin_enqueue_scripts)
    *  @since   3.6
    *  @date    23/01/13
    *
    *  @param   n/a
    *  @return  n/a
    */

    
    
    function input_admin_enqueue_scripts() {
        
        // vars
        $url = $this->settings['url'];
        $version = $this->settings['version'];

        // register & include JS
        wp_register_script( 'acf-input-custom_autocomplete', "{$url}assets/js/input.js", array('acf-input', 'jquery-ui-autocomplete'), $version );
        wp_enqueue_script('acf-input-custom_autocomplete');
        
        // register & include CSS
        wp_register_style( 'acf-input-custom_autocomplete', "{$url}assets/css/input.css", array('acf-input'), $version );
        wp_enqueue_style('acf-input-custom_autocomplete');
        
    }

    function format_value($value, $post_id, $field)
    {
        global $wpdb;
        $query = $wpdb->prepare("SELECT * FROM {$field['custom_db_table']} WHERE id = %d", $value);
        return $wpdb->get_row($query);
    }

    function update_value($value, $post_id, $field)
    {
        if(empty($value))
            return $value;
        if(!isset($_POST['acf'][$field['key']])) // Repeater?
            return $this->getValueInRepeater($value, $_POST, $field);

        if(!is_array($_POST['acf'][$field['key']])) // Not a repeater field
            return $this->getValueInSingle($_POST, $field);

        return $value;
    }

    /**
     * @since 1.2.0 (custom-autocomplete)
     *
     * @param $value (string)
     * @param $post (array)
     * @param $field (array)
     * @return mixed
     */
    public function getValueInRepeater($value, $post, $field)
    {
        $key = array_keys($post['acf'][$field['parent']], [$field['key'] => $value]);
        $last_piece_of_value_array_key = 'acf-' . $field['parent'] . '-' . $key[0] . '-' . $field['key'];
        return $post['custom-autocomplete'][$field['key']][$last_piece_of_value_array_key];
    }

    /**
     * @since 1.2.0 (custom-autocomplete)
     *
     * @param $post (array)
     * @param $field (array)
     * @return mixed
     */
    public function getValueInSingle($post, $field)
    {
        $last_piece_of_value_array_key = 'acf-'.$field['key'];
        return $post['custom-autocomplete'][$field['key']][$last_piece_of_value_array_key];
    }
}


// initialize
new acf_field_custom_autocomplete( $this->settings );


// class_exists check
endif;

?>