<?php

namespace WordPress\Pixo\SharedAutoComplete;

class AjaxAutoComplete
{
    protected $db;
    protected $table_name;
    protected $cols;
    protected $cols_select;
    protected $field;
    protected $search;
    protected $raw_results;
    protected $formatted_results;
    protected $limit;
    protected $where_filter;

    public function __construct($search, $field)
    {
        global $wpdb;
        $this->db = $wpdb;
        $this->search = $search;
        $this->setTableName($field['custom_db_table']);
        $this->setCols($field['db_columns']);
        $this->setColsSelect($field['db_columns_select']);
        $this->setField($field);
        $this->setLimit($field['limit']);
        $this->setWhereFilter($field['where_filter']);
        $this->query();
        $this->formatResults();

        wp_die(wp_json_encode($this->formatted_results));
    }

    protected function setTableName($table_name)
    {
        $this->table_name = $table_name;
    }

    protected function setCols($cols)
    {
        $cols = str_replace(' ', '', (string) $cols);
        $this->cols = explode(',', $cols);
    }

    protected function setColsSelect($cols)
    {
        $this->cols_select = (string) $cols;
    }

    protected function setField($field)
    {
        $this->field = (array) $field;
    }

    protected function setLimit($limit)
    {
        $this->limit = (int) $limit;
    }

    protected function setWhereFilter($filter)
    {
        $this->where_filter = $filter;
    }

    protected function query()
    {
        $query = "SELECT {$this->cols_select} FROM {$this->table_name}";
        $query .= " WHERE " . $this->getSearchSql($this->cols, 'both');
        if(!empty($this->where_filter))
            $query .= " AND $this->where_filter";
        $query .= " LIMIT $this->limit";
        $this->raw_results = $this->db->get_results($query);
    }

    /**
     * We use the id_column and label_column params on the field to know what in results to
     * use for value and label.  People wishing to modify this can use any of the four filter
     * hooks provided at the end of this method:
     *      pixo_shared_autocomplete_post_format_results
     *      pixo_shared_autocomplete_post_format_results/db_table={$this->table_name}
     *      pixo_shared_autocomplete_post_format_results/name={$this->field['name']}
     *      pixo_shared_autocomplete_post_format_results/key={$this->field['key']}
     *
     * Use the filters to modify the formatted results.
     */
    protected function formatResults()
    {
        $formatted = [];
        if(is_array($this->raw_results)) {
            foreach ($this->raw_results as $k => $v) {
                $respObj = new \stdClass;
                $respObj->value = $v->{$this->field['id_column']};
                $respObj->label = $v->{$this->field['label_column']};
                $formatted[] = $respObj;
            }
        }

        $formatted = apply_filters('pixo_shared_autocomplete_post_format_results', $formatted, $this->raw_results);
        $formatted = apply_filters("pixo_shared_autocomplete_post_format_results/db_table={$this->table_name}", $formatted, $this->raw_results);
        $formatted = apply_filters("pixo_shared_autocomplete_post_format_results/name={$this->field['name']}", $formatted, $this->raw_results);
        $formatted = apply_filters("pixo_shared_autocomplete_post_format_results/key={$this->field['key']}", $formatted, $this->raw_results);
        
        $this->formatted_results = $formatted;
    }

    /**
     * Copied from core WP_User_Query class, defined 
     * in wp-includes/class-wp-user-query.php.
     */
    protected function getSearchSql($cols, $wild = false)
    {
        $searches = array();
        $leading_wild = ( 'leading' == $wild || 'both' == $wild ) ? '%' : '';
        $trailing_wild = ( 'trailing' == $wild || 'both' == $wild ) ? '%' : '';
        $like = $leading_wild . $this->db->esc_like( $this->search ) . $trailing_wild;
     
        foreach ( $cols as $col ) {
            if ( 'ID' == $col ) {
                $searches[] = $this->db->prepare( "$col = %s", $this->search );
            } else {
                $searches[] = $this->db->prepare( "$col LIKE %s", $like );
            }
        }
     
        return '(' . implode(' OR ', $searches) . ')';
    }
}